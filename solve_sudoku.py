############################################################################################################
#RENAN PEREIRA 2021
############################################################################################################
#Solving an NxN sudoku puzzle with a genetic algorithm.

import math
import matplotlib
import random
matplotlib.use('Agg')
import matplotlib.pyplot as plt


def takeSecond(elem):
    return elem[1]


#Read sudoku from dat file
def read_sudoku_from_file(sudoku_file_name):
	file_sudoku = open(sudoku_file_name, "r")

	sudoku_lines=[]
	for line in file_sudoku:
		sudoku_lines.append(line)

	file_sudoku.close()

	#Write sudoku on a matrix
	sudoku_matrix=[[0 for x in range(0,len(sudoku_lines))] for y in range(0,len(sudoku_lines))] 
	for i in range(0,len(sudoku_lines)):
		for j in range(0,len(sudoku_lines)):
			sudoku_matrix[i][j] = int( sudoku_lines[i].rstrip('\n').split(',')[j] )

	return sudoku_matrix



#Print a sudoku on the console
def print_sudoku(sudoku):
	for i in range(0,len(sudoku)):
		for j in range(0,len(sudoku[i])):
			print(sudoku[i][j], end = ' ')
		print()
	print()


#Return the number of clues in the sudoku
def number_of_clues(sudoku_skeleton):
	clues = 0
	for i in range(0,len(sudoku_skeleton)):
		for j in range(0,len(sudoku_skeleton)):
			if (sudoku_skeleton[i][j]!=0):
				clues = clues + 1

	return clues


#Function that generates a sudoku given an initial sudoku filled with clues
def generate_sudoku_by_lines(sudoku_skeleton):
	sudoku_full = [[0 for x in range(0,len(sudoku_skeleton))] for y in range(0,len(sudoku_skeleton))] 

	for i in range(0,len(sudoku_skeleton)):
		line_elements = []
		guesses = []
		for j in range(0,len(sudoku_skeleton)):
			line_elements.append(sudoku_skeleton[i][j])
			guesses.append(j+1)

		for j in range(0,len(line_elements)):
			if line_elements[j]!=0:
				guesses.remove(line_elements[j])

		for j in range(0,len(sudoku_skeleton)):
			if (sudoku_skeleton[i][j]==0):
				aux = random.choice(guesses) 
				sudoku_full[i][j] = aux
				guesses.remove(aux)
			else:
				sudoku_full[i][j] = sudoku_skeleton[i][j]

	return sudoku_full


#Function that calculate the lines weight of a given sudoku
def sudoku_line_weight(sudoku, line):
	mold = [x+1 for x in range(0,len(sudoku_matrix))]
	count = [0 for x in range(0,len(sudoku))]

	for i in range(0,len(sudoku)):
		sudoku_element = sudoku[line][i] 
		for j in range(0,len(mold)):
			if (sudoku_element==mold[j]):
				count[j] = count[j] + 1

	weight = 0
	for i in range(0,len(count)):
		weight = weight + (count[i]-1)**2

	return weight



#Function that calculate the columns weight of a given sudoku
def sudoku_column_weight(sudoku, column):
	mold = [x+1 for x in range(0,len(sudoku))]
	count = [0 for x in range(0,len(sudoku))]

	for i in range(0,len(sudoku)):
		sudoku_element = sudoku[i][column] 
		for j in range(0,len(mold)):
			if (sudoku_element==mold[j]):
				count[j] = count[j] + 1

	weight = 0
	for i in range(0,len(count)):
		weight = weight + (count[i]-1)**2

	return weight


#Function that calculate the block weight of a given sudoku
def sudoku_block_weight(sudoku, block_line, block_column):
	mold = [x+1 for x in range(0,len(sudoku))]
	count = [0 for x in range(0,len(sudoku))]

	minor_square_size = int(math.sqrt(len(sudoku)))

	X=block_line*minor_square_size
	Y=block_column*minor_square_size
			
	column_elements = []
	for i in range(X,X+minor_square_size):
		for j in range(Y,Y+minor_square_size):
			column_elements.append(sudoku[i][j])

	for i in range(0,len(sudoku)):
		sudoku_element = column_elements[i]
		for j in range(0,len(mold)):
			if (sudoku_element==mold[j]):
				count[j] = count[j] + 1

	weight = 0
	for i in range(0,len(count)):
	 	weight = weight + (count[i]-1)**2

	return weight



#Function that calculate the lines observable of a given sudoku
def sudoku_lines_OBS(sudoku):
	obs = 0
	for i in range(0,len(sudoku)):
		obs = obs + sudoku_line_weight(sudoku, i)

	return obs


#Function that calculate the columns observable of a given sudoku
def sudoku_columns_OBS(sudoku):
	obs = 0
	for i in range(0,len(sudoku)):
		obs = obs + sudoku_column_weight(sudoku, i)

	return obs


#Function that calculate the blocks observable of a given sudoku
def sudoku_blocks_OBS(sudoku):
	obs = 0
	minor_square_size = int(math.sqrt(len(sudoku)))

	for L in range(0,minor_square_size):
		for C in range(0,minor_square_size):
			obs = obs + sudoku_block_weight(sudoku, L, C)

	return obs


#Function that calculates the total weight of a sudoku
def weight_sudoku_configuration(sudoku):
	W = 0
	W = W + sudoku_columns_OBS(sudoku)
	W = W + sudoku_blocks_OBS(sudoku)

	return W


#Calculate population weight
def population_weight(population):
	pop_weight = []
	for i in range(0,len(population)):
		pop_weight.append( [i, weight_sudoku_configuration(population[i])] )

	return pop_weight



def sudoku_tournament(sudoku_population):

	warrior_1 = random.randint(0,len(sudoku_population)-1)
	warrior_2 = random.randint(0,len(sudoku_population)-1)
	while(warrior_2==warrior_1):
		warrior_2 = random.randint(0,len(sudoku_population)-1)

	weight_warrior_1 = weight_sudoku_configuration(sudoku_population[warrior_1])
	weight_warrior_2 = weight_sudoku_configuration(sudoku_population[warrior_2])

	if weight_warrior_1<weight_warrior_2:
		winner = warrior_1
	else:
		winner = warrior_2

	return winner


#Increase the population by generating descendants
def generate_descendents(N_population, N_descendents, sudoku_skeleton, sudoku_population):

	if (N_population!=len(sudoku_population)):
		print("Problem: Given population size does not match the provided population.")

	sudoku_population_descendents=[]
	for i in range(0,N_descendents):
		#create couple
		partners = []
		#partners.append( random.randint(0,N_population-1) )
		#partners.append( random.randint(0,N_population-1) )
		partners.append( sudoku_tournament(sudoku_population) )
		partners.append( sudoku_tournament(sudoku_population) )
		while(partners[0]==partners[1]):
			partners[1] = sudoku_tournament(sudoku_population)

		sudoku_descendent = []
		for j in range(0, len(sudoku_skeleton)):
			parent = random.choice(partners)
			sudoku_descendent.append(sudoku_population[parent][j])
	
		sudoku_population_descendents.append(sudoku_descendent)

	return sudoku_population_descendents


#After the population generates descedents, only the fittest surivive
def survival_of_the_fittest(N_OG_population, N_descendents, sudoku_population_weight, sudoku_population):

	sudoku_population_weight.sort(key=takeSecond,reverse=False)

	sudoku_NEW_population=[]
	for i in range(0,N_OG_population):
		sudoku_surviver_index = sudoku_population_weight[i][0]
		sudoku_NEW_population.append( sudoku_population[sudoku_surviver_index] )

	sudoku_population = sudoku_NEW_population

	return sudoku_population


def mutate_sudoku(sudoku_skeleton, sudoku):
	#calculate the maximum number of mutations
	max_number_mutations = int( ( len(sudoku_skeleton)**2 - number_of_clues(sudoku_skeleton) ) )

	#calculate the number of mutations randomly
	number_mutations = random.randint(0, max_number_mutations )

	sudoku_mutated = [[0 for x in range(0,len(sudoku_skeleton))] for y in range(0,len(sudoku_skeleton))] 
	for i in range(0,len(sudoku)):
		for j in range(0,len(sudoku)):
			sudoku_mutated[i][j] = sudoku[i][j]

	for i in range(0,number_mutations):

		#randomly choose a line to mutate
		L = random.randint(0,len(sudoku_skeleton)-1)

		#randomly choose two elements in the line to exchange position
		generate = True
		while (generate==True):
			C1 = random.randint(0,len(sudoku_skeleton)-1)
			C2 = C1
			while (C2==C1):
				C2 = random.randint(0,len(sudoku_skeleton)-1)

			if (sudoku_skeleton[L][C1]==0 and sudoku_skeleton[L][C2]==0):
				sudoku_weight_before_mutation = weight_sudoku_configuration(sudoku_mutated)
				aux1 = sudoku_mutated[L][C1]
				aux2 = sudoku_mutated[L][C2]
				sudoku_mutated[L][C1] = aux2
				sudoku_mutated[L][C2] = aux1
				sudoku_weight_after_mutation = weight_sudoku_configuration(sudoku_mutated)
				if sudoku_weight_after_mutation>sudoku_weight_before_mutation:
					sudoku_mutated[L][C1] = aux1
					sudoku_mutated[L][C2] = aux2
				generate=False

	return sudoku_mutated


############################################################################################################
############################################################################################################
#CODE


#Read the sudoku from the file
sudoku_matrix = read_sudoku_from_file("sudoku_example1.dat")


#Print sudoku
print_sudoku(sudoku_matrix)

	
#Print number of clues in the sudoku
print( "Number of clues:", number_of_clues(sudoku_matrix), "\n" )



#Generate sudoku population
sudoku_population = []
N = 50#500
N_births_from_vacuum = 10
N_descendents = 20
M = N

N_line_population = N
for i in range(0,N_line_population):
	sudoku_population.append( generate_sudoku_by_lines(sudoku_matrix) )


#Calculate weight of the complete OG population
sudoku_pop_weight = []
sudoku_pop_weight = population_weight(sudoku_population)
sudoku_pop_weight.sort(key=takeSecond,reverse=False)
print(sudoku_pop_weight)


generations = 350

for g in range(0,generations):

	print("generation: ", g, "\n")

	for i in range(0,N_births_from_vacuum):
		sudoku_population.append( generate_sudoku_by_lines(sudoku_matrix) )

	#Generate descendents
	sudoku_population_descendents = []
	sudoku_population_descendents = generate_descendents(N+N_births_from_vacuum, N_descendents, sudoku_matrix, sudoku_population)
	
	#Mutate descendents
	for i in range(0,len(sudoku_population_descendents)):
		sudoku_population_descendents[i] = mutate_sudoku(sudoku_matrix, sudoku_population_descendents[i])

	#Mutate some individuals
	for i in range(0, 200):
		x = random.randint(0,len(sudoku_population)-1)
		sudoku_population[x] = mutate_sudoku(sudoku_matrix, sudoku_population[x])

	#Add descendents to population
	for i in range(0,len(sudoku_population_descendents)):
		sudoku_population.append( sudoku_population_descendents[i] )

	#Calculate cost function of the complete population
	sudoku_pop_weight = []
	sudoku_pop_weight = population_weight(sudoku_population)
	sudoku_pop_weight.sort(key=takeSecond,reverse=False)

	print(len(sudoku_population))

	#Select only the "fittest" individuals
	sudoku_population = survival_of_the_fittest(N, M, sudoku_pop_weight, sudoku_population)

	print(len(sudoku_population))

	neo_weight = weight_sudoku_configuration(sudoku_population[0])
	print("weight of the fittest individual: ", neo_weight)
	if neo_weight==0:
		break



sudoku_pop_weight = []
sudoku_pop_weight = population_weight(sudoku_population)
sudoku_pop_weight.sort(key=takeSecond,reverse=False)
print(sudoku_pop_weight, "\n")


print_sudoku(sudoku_population[0])

